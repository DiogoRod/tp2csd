package bftsmart;

import bftsmart.tom.MessageContext;
import bftsmart.tom.ServiceReplica;
import bftsmart.tom.server.Recoverable;
import bftsmart.tom.server.defaultservices.DefaultRecoverable;
import java.io.*;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import org.json.JSONException;

/**
 * Created by dcrod on 14/04/2017.
 */
public class BftServer extends DefaultRecoverable implements Recoverable {

	ServiceReplica replica = null;
	KVStore kvstore = null;
	static boolean bad = false;
	int id;

	public BftServer(int id) {
		kvstore = new KVStore();
		replica = new ServiceReplica(id, this, this);
		this.id = id;
	}

	public static void main(String[] args) throws IOException {
		int idI = Integer.parseInt(args[0]);
		if (args[1].equals("y"))
			bad = true;
		new BftServer(idI);
	}

	@Override
	public byte[][] appExecuteBatch(byte[][] command, MessageContext[] mcs) {

		byte[][] replies = new byte[command.length][];
		for (int i = 0; i < command.length; i++) {
			replies[i] = executeSingle(command[i], mcs[i]);
		}
		return replies;
	}

	private byte[] executeSingle(byte[] command, MessageContext msgCtx) {
		try {
			command = decompress(command);
		} catch (IOException | DataFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ByteArrayInputStream in = new ByteArrayInputStream(command);
		DataInputStream dis = new DataInputStream(in);
		int reqType;
		byte[] reply = null;
		try {
			reqType = dis.readInt();
			switch (reqType) {
			case BftRequestType.TEST:
				int i = dis.readInt();
				int value = kvstore.test(i, bad);

				reply = new byte[] { (byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value };
				break;

			case BftRequestType.PUT:

				String key = dis.readUTF();
				String entry = dis.readUTF();
				kvstore.putSet(key, entry);
				reply = entry.getBytes();
				break;

			case BftRequestType.ADD:
				String key1 = dis.readUTF();
				String obj = dis.readUTF();
				kvstore.addElem(key1, obj);
				reply = obj.getBytes();
				break;

			case BftRequestType.REMOVE:
				String key2 = dis.readUTF();
				kvstore.removeSet(key2);
				reply = key2.getBytes();
				break;
			case BftRequestType.WRITE:
				String key3 = dis.readUTF();
				String obj1 = dis.readUTF();
				kvstore.writeElem(key3, obj1);
				reply = key3.getBytes();
				break;
			case BftRequestType.GET:
				String key4 = dis.readUTF();
				reply = kvstore.getSet(key4, bad).getBytes();

				break;
			case BftRequestType.READ:
				String key5 = dis.readUTF();
				String obj2 = dis.readUTF();
				reply = kvstore.readElem(key5, obj2).getBytes();
				break;
			case BftRequestType.ISELEM:
				String key6 = dis.readUTF();
				String elem = dis.readUTF();
				reply = kvstore.isElem(key6, elem).getBytes();
				break;
			case BftRequestType.SUM:
				String key7 = dis.readUTF();
				String key8 = dis.readUTF();
				reply = kvstore.sum(key7, key8).getBytes();
				break;
			case BftRequestType.SUMALL:
				reply = kvstore.sumAll().getBytes();
				break;
			case BftRequestType.MULT:
				String key9 = dis.readUTF();
				String key10 = dis.readUTF();
				reply = kvstore.mult(key9, key10).getBytes();
				break;
			case BftRequestType.MULTALL:
				reply = kvstore.multAll().getBytes();
				break;
			case BftRequestType.SEARCHEQ:
				String value0 = dis.readUTF();
				reply = kvstore.searchEq(value0).getBytes();
				break;
			case BftRequestType.SEARCHNQ:
				String value1 = dis.readUTF();
				reply = kvstore.searchNq(value1).getBytes();
				break;
			case BftRequestType.ORDERLS:
				reply = kvstore.orderLS().getBytes();
				break;
			case BftRequestType.ORDERSL:
				reply = kvstore.orderSL().getBytes();
				break;
			case BftRequestType.SEARCHGT:
				String value2 = dis.readUTF();
				reply = kvstore.searchGt(value2).getBytes();
				break;
			case BftRequestType.SEARCHGTEQ:
				String value3 = dis.readUTF();
				reply = kvstore.searchGtEq(value3).getBytes();
				break;
			case BftRequestType.SEARCHLT:
				String value4 = dis.readUTF();
				reply = kvstore.searchLt(value4).getBytes();
				break;
			case BftRequestType.SEARCHLTEQ:
				String value5 = dis.readUTF();
				reply = kvstore.searchLtEq(value5).getBytes();
				break;
			case BftRequestType.INSTALLSNAP:
				byte[] snap = new byte[command.length - Integer.BYTES];
				dis.read(snap);
				installSnapshot(snap);
				reply = "Installed".getBytes();
				break;
			}

			return reply;
		} catch (IOException e) {
			System.out.println("Erro no executeSingle: " + e.getMessage());
			e.printStackTrace();
		} /*
			 * catch (JSONException e) { e.printStackTrace(); }
			 */ catch (Exception e) {
			e.printStackTrace();
		}
		return "null".getBytes();
	}

	@Override
	public byte[] appExecuteUnordered(byte[] bytes, MessageContext messageContext) {
		try {
			bytes = decompress(bytes);
			ByteArrayInputStream in = new ByteArrayInputStream(bytes);
			DataInputStream dis = new DataInputStream(in);
			byte[] reply = null;
			int reqType = dis.readInt();
			switch (reqType) {

			case BftRequestType.GET:
				String key = dis.readUTF();
				try {
					reply = kvstore.getSet(key, bad).getBytes();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (reply == null)
					reply = key.getBytes();
				break;
			case BftRequestType.READ:
				String key1 = dis.readUTF();
				String obj = dis.readUTF();
				try {
					reply = kvstore.readElem(key1, obj).getBytes();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case BftRequestType.ISELEM:
				String key2 = dis.readUTF();
				String elem = dis.readUTF();
				reply = kvstore.isElem(key2, elem).getBytes();
				break;
			case BftRequestType.SEARCHEQ:
				String value = dis.readUTF();
				try {
					reply = kvstore.searchEq(value).getBytes();
				} catch (ClassNotFoundException | JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case BftRequestType.SEARCHNQ:
				String value1 = dis.readUTF();
				reply = kvstore.searchNq(value1).getBytes();
				break;
			case BftRequestType.ORDERLS:
				try {
					reply = kvstore.orderLS().getBytes();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case BftRequestType.ORDERSL:
				try {
					reply = kvstore.orderSL().getBytes();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case BftRequestType.SEARCHGT:
				String value2 = dis.readUTF();
				reply = kvstore.searchGt(value2).getBytes();
				break;
			case BftRequestType.SEARCHGTEQ:
				String value3 = dis.readUTF();
				reply = kvstore.searchGtEq(value3).getBytes();
				break;
			case BftRequestType.SEARCHLT:
				String value4 = dis.readUTF();
				reply = kvstore.searchLt(value4).getBytes();
				break;
			case BftRequestType.SEARCHLTEQ:
				String value5 = dis.readUTF();
				reply = kvstore.searchLtEq(value5).getBytes();
				break;
			case BftRequestType.GETSNAP:
				reply = getSnapshot();
				break;

			}
			if (reply == null)
				return "null".getBytes();
			else
				return reply;

		} catch (IOException | DataFormatException e) {
			e.printStackTrace();
		} /*
			 * catch (JSONException e) { e.printStackTrace(); } catch
			 * (ClassNotFoundException e) { System.out.println("404"); }
			 */
		return new byte[0];
	}

	@Override
	public void installSnapshot(byte[] bytes) {
		ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
		try {
			ObjectInput in = new ObjectInputStream(bis);
			kvstore = (KVStore) in.readObject();
			in.close();
			bis.close();
		} catch (ClassNotFoundException e) {
			System.out.print("Coudn't find Map: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.print("Exception installing the application state: " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public byte[] getSnapshot() {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(bos);
			out.writeObject(kvstore);
			out.flush();
			out.close();
			bos.close();
			return bos.toByteArray();
		} catch (IOException e) {
			System.out.println(
					"Exception when trying to take a + " + "snapshot of the application state" + e.getMessage());
			e.printStackTrace();
			return new byte[0];
		}
		// return new byte[0];
	}

	public static byte[] decompress(byte[] data) throws IOException, DataFormatException {
		Inflater inflater = new Inflater();
		inflater.setInput(data);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		while (!inflater.finished()) {
			int count = inflater.inflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		outputStream.close();
		byte[] output = outputStream.toByteArray();
		return output;
	}

}