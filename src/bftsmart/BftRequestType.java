package bftsmart;

/**
 * Created by dcrod on 14/04/2017.
 */
public class BftRequestType {
    public static final int PUT =1;
    public static final int GET =2;
    public static final int ADD =3;
    public static final int REMOVE =4;
    public static final int WRITE =5;
    public static final int READ =6;
    public static final int ISELEM =7;
    public static final int SUM =8;
    public static final int SUMALL =9;
    public static final int MULT =10;
    public static final int MULTALL =11;
    public static final int UPDATE =12;
	public static final int ALIVE = 14;
    public static final int SEARCHEQ = 15;
    public static final int SEARCHNQ = 16;
    public static final int SEARCHLTEQ = 17;
    public static final int SEARCHGTEQ = 18;
    public static final int SEARCHLT = 19;
    public static final int SEARCHGT = 20;
    public static final int ORDERSL = 21;
    public static final int ORDERLS = 22;
    public static final int SEARCHENTRY = 23;
    public static final int SEARCHOR = 24;
    public static final int SEARCHAND = 25;
	public static final int GETSNAP = 0;
	public static final int INSTALLSNAP = 30;
	public static final int TEST = 100;
















}
