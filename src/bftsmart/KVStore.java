package bftsmart;

import clt.KeyDispenser;
import mlib.HomoAdd;
import mlib.HomoDet;
import mlib.HomoMult;
import mlib.HomoOpeInt;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.Map.Entry;

/**
 * Created by diogo on 15/04/17.
 */
public class KVStore implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8764406757301993385L;
	private TreeMap<String, String[]> employeeData = null;
	private TreeMap<Integer, Integer> testTree = null;

	public KVStore() {
		employeeData = new TreeMap<>();
		testTree = new TreeMap<>();
	}

	public void putSet(String key, String obj) throws JSONException {
		JSONObject jobj = new JSONObject(obj);
		//ArrayList<String> employee = new ArrayList<String>();
		String[] employee = new String[200];
		for (int i = 0; i < jobj.length(); i++) {
			employee[i]= (jobj.getString("elem" + i));
		}
		/*String[] aEmployee = new String[employee.size()];
		employee.toArray(aEmployee);*/
		employeeData.put(key, employee);
		// System.out.println("Before put: key: "+key+" obj: "+obj);
		 System.out.println("After put: "+ employeeData.get(key));
	}

	public String getSet(String key, boolean bad) throws JSONException {
		if (bad) {
			System.out.println("BAD VALUE: " + bad);
			System.out.println("Normal: " + employeeData.get(key));
			return "{}";
		} else {
			if (employeeData.get(key) != null) {
				JSONObject obj = new JSONObject();
				for (int i = 0; i < employeeData.get(key).length; i++) {
					obj.put("elem" + i, employeeData.get(key)[i]);
				}
				System.out.println(obj.toString());
				return obj.toString();
			} else
				System.out.println("null");
				return "null";
		}
	}

	public void addElem(String key, String obj) throws JSONException {
		int count = 0;
		for (int i = 0; i < employeeData.get(key).length; i++) {
			if (employeeData.get(key)[i] != null)
				count++;
		}
		// System.out.println("Before add: "+ employeeData.get(key)[count]);
		employeeData.get(key)[count] = obj;
		System.out.println("After add: " + employeeData.get(key)[count]);
	}

	public void removeSet(String key) {
		employeeData.remove(key);
	}

	public void writeElem(String key, String obj) throws JSONException {
		JSONObject jobj = new JSONObject(obj);
		int pos = jobj.getInt("elem0");
		String newValue = jobj.getString("elem1");
		employeeData.get(key)[pos] = newValue;

	}

	public String readElem(String key, String obj) throws JSONException {
		int pos = Integer.parseInt(obj);
		return employeeData.get(key)[pos];
	}

	public String isElem(String key, String obj) {
		if (!employeeData.containsKey(key))
			return "Entry not available";
		return String.valueOf(HomoDet.compare(employeeData.get(key)[0], obj));
	}

	public String sum(String key, String key1) throws Exception {
		KeyDispenser kd = new KeyDispenser();
		BigInteger left = new BigInteger(employeeData.get(key)[3]);
		BigInteger right = new BigInteger(employeeData.get(key1)[3]);
		BigInteger nsq = new BigInteger(kd.getNSq());
		BigInteger result = HomoAdd.sum(left, right, nsq);
		return result.toString();
	}

	public String sumAll() throws IOException, ClassNotFoundException {
		KeyDispenser kd = new KeyDispenser();
		ArrayList<String> values = getArray(3);

		if (values.size() < 2) {
			return values.get(0);
		} else if (values.size() == 2) {
			return HomoAdd
					.sum(new BigInteger(values.get(0)), new BigInteger(values.get(1)), new BigInteger(kd.getNSq()))
					.toString();
		} else {
			BigInteger temp = HomoAdd.sum(new BigInteger(values.get(0)), new BigInteger(values.get(1)),
					new BigInteger(kd.getNSq()));
			for (int i = 3; i < values.size(); i++) {
				temp = HomoAdd.sum(temp, new BigInteger(values.get(i)), new BigInteger(kd.getNSq()));
			}
			return temp.toString();
		}
	}

	public String mult(String key, String key1)
			throws IOException, ClassNotFoundException, InvalidKeySpecException, NoSuchAlgorithmException {
		KeyDispenser kd = new KeyDispenser();
		System.out.println(HomoMult.decrypt(kd.getPrivateKey(), new BigInteger(employeeData.get(key)[2])));
		System.out.println(HomoMult.decrypt(kd.getPrivateKey(), new BigInteger(employeeData.get(key1)[2])));
		BigInteger produto = HomoMult.multiply(new BigInteger(employeeData.get(key)[2]),
				new BigInteger(employeeData.get(key1)[2]), kd.getPublicKey());
		return produto.toString();
	}

	public String multAll()
			throws IOException, ClassNotFoundException, InvalidKeySpecException, NoSuchAlgorithmException {
		KeyDispenser kd = new KeyDispenser();
		ArrayList<String> values = getArray(2);
		if (values.size() < 2) {
			return values.get(0);
		} else if (values.size() == 2) {
			return HomoMult.multiply(new BigInteger(values.get(0)), new BigInteger(values.get(1)), kd.getPublicKey())
					.toString();
		} else {
			BigInteger temp = HomoMult.multiply(new BigInteger(values.get(0)), new BigInteger(values.get(1)),
					kd.getPublicKey());
			for (int i = 3; i < values.size(); i++) {
				temp = HomoMult.multiply(temp, new BigInteger(values.get(i)), kd.getPublicKey());
			}
			return temp.toString();
		}
	}

	public String searchEq(String value) throws JSONException, IOException, ClassNotFoundException {
		try {
			Map<String, String[]> tmpData = employeeData;
			Iterator<Entry<String, String[]>> it = tmpData.entrySet().iterator();
			JSONObject obj = new JSONObject();
			int c = 0;
			while (it.hasNext()) {
				System.out.println("Entrou no while");
				Map.Entry pair = (Map.Entry) it.next();
				String key = (String) pair.getKey();
				if (tmpData.containsKey(key)) {
					System.out.println(tmpData.get(key)[0] + " : " + value);
					if (HomoDet.compare(tmpData.get(key)[0], value)) {
						System.out.println("encontrou igual");
						System.out.println(obj.toString());
						obj.put("elem" + c, getSet(key, false));
						c++;
					} // it.remove();
				}
			}
			System.out.println(isJSONValid(obj.toString()));
			return obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "nenhum";
	}

	public String searchNq(String value) {
		try {
			Iterator it = employeeData.entrySet().iterator();
			JSONObject obj = new JSONObject();
			int c = 0;
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				String key = (String) pair.getKey();
				if (employeeData.containsKey(key)) {
					if (!(HomoDet.compare(employeeData.get(key)[0], value))) {
						System.out.println("encontrou diferente");
						System.out.println(obj.toString());
						obj.put("elem" + c, getSet(key, false));
						c++;
					} // it.remove();
				}
			}
			System.out.println(isJSONValid(obj.toString()));
			return obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "nenhum";
	}

	public String orderLS() throws JSONException {

		ArrayList<String> salaries = getArray(1);
		Collections.sort(salaries, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				HomoOpeInt ope1 = new HomoOpeInt();
				if (ope1.compare(Long.parseLong(o1), Long.parseLong(o2))) {
					return 1;
				} else if (o1.equals(o2)) {
					return 0;
				} else {
					return -1;
				}
			}
		});
		JSONObject obj = new JSONObject();
		for (int i = 0; i < salaries.size(); i++) {
			obj.put("elem" + i, salaries.get(i));
		}

		return obj.toString();
	}

	public String orderSL() throws JSONException {

		ArrayList<String> salaries = getArray(1);
		Collections.sort(salaries, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				HomoOpeInt ope1 = new HomoOpeInt();
				if (ope1.compare(Long.parseLong(o1), Long.parseLong(o2))) {
					return -1;
				} else if (o1.equals(o2)) {
					return 0;
				} else {
					return 1;
				}
			}
		});
		JSONObject obj = new JSONObject();
		for (int i = 0; i < salaries.size(); i++) {
			obj.put("elem" + i, salaries.get(i));
		}

		return obj.toString();
	}

	public String searchGt(String value) {
		try {
			HomoOpeInt ope = new HomoOpeInt();
			Iterator it = employeeData.entrySet().iterator();
			JSONObject obj = new JSONObject();
			int c = 0;
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				String key = (String) pair.getKey();
				if (employeeData.containsKey(key)) {
					if (ope.compare(Long.parseLong(employeeData.get(key)[1]), Long.parseLong(value))) {
						obj.put("elem" + c, getSet(key, false));
						c++;
					} //
					//it.remove();
				}
			}
			System.out.println(isJSONValid(obj.toString()));
			return obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "nenhum";
	}

	public String searchGtEq(String value) {
		try {
			HomoOpeInt ope = new HomoOpeInt();
			Iterator it = employeeData.entrySet().iterator();
			JSONObject obj = new JSONObject();
			int c = 0;
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				String key = (String) pair.getKey();
				if (employeeData.containsKey(key)) {
					long l1 = Long.parseLong(employeeData.get(key)[1]);
					long l2 = Long.parseLong(value);
					if ((ope.compare(l1, l2)) || l1 == l2) {
						obj.put("elem" + c, getSet(key, false));
						c++;
					} // it.remove();
				}
			}
			System.out.println(isJSONValid(obj.toString()));
			return obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "nenhum";
	}

	public String searchLt(String value) {
		try {
			HomoOpeInt ope = new HomoOpeInt();
			Iterator it = employeeData.entrySet().iterator();
			JSONObject obj = new JSONObject();
			int c = 0;
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				String key = (String) pair.getKey();
				if (employeeData.containsKey(key)) {
					if (ope.compare(Long.parseLong(value), Long.parseLong(employeeData.get(key)[1]))) {
						obj.put("elem" + c, getSet(key, false));
						c++;
					} // it.remove();
				}
			}
			System.out.println(isJSONValid(obj.toString()));
			return obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "nenhum";
	}

	public String searchLtEq(String value) {
		try {
			HomoOpeInt ope = new HomoOpeInt();
			Iterator it = employeeData.entrySet().iterator();
			JSONObject obj = new JSONObject();
			int c = 0;
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				String key = (String) pair.getKey();
				if (employeeData.containsKey(key)) {
					long l1 = Long.parseLong(employeeData.get(key)[1]);
					long l2 = Long.parseLong(value);
					System.out.println(l1 + " " + l2);
					if ((ope.compare(l2, l1)) || l1 == l2) {
						String s = getSet(key, false);
						System.out.println("S: " + s);
						obj.put("elem" + c, s);
						c++;
					} //
					//it.remove();
				}
			}
			System.out.println(isJSONValid(obj.toString()));
			return obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "nenhum";
	}

	public ArrayList<String> getArray(int column) {
		Map<String, String[]> tmpData = employeeData;
		Iterator it = tmpData.entrySet().iterator();
		ArrayList<String> values = new ArrayList<>();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			String key = (String) pair.getKey();
			values.add(tmpData.get(key)[column]); //
			// it.remove(); // avoids a ConcurrentModificationException
		}
		return values;
	}

	public boolean isJSONValid(String test) {
		try {
			new JSONObject(test);
		} catch (JSONException ex) {
			try {
				new JSONArray(test);
			} catch (JSONException ex1) {
				return false;
			}
		}
		return true;
	}

	public int test(int i, boolean bad) {
		testTree.put(i, i);
		System.out.println(i);
		if (bad)
			return i + 1;
		return i;
	}
}
