package bftsmart;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class Replica {
	public static final int START_REQUEST = 1;
	public static final int REMOVE_REQUEST = 2;
	public static final int ALIVE_REQUEST = 0;
	public static final int PORT = 6060;
	static final File KEYSTORE = new File("./server.jks");
	static final char[] JKS_PASSWORD = "changeit".toCharArray();
	static final char[] KEY_PASSWORD = "changeit".toCharArray();
	private static final String CLASSPATH = "C:\\Users\\Shiro\\git\\csdtp2\\target\\classes;C:\\Users\\Shiro\\Desktop\\library-1.1-beta\\bin\\BFT-SMaRt.jar;C:\\Users\\Shiro\\git\\csdtp2\\lib\\commons-codec-1.5.jar;C:\\Users\\Shiro\\git\\csdtp2\\lib\\core-0.1.4.jar;C:\\Users\\Shiro\\git\\csdtp2\\lib\\netty-all-4.0.25.Final.jar;C:\\Users\\Shiro\\git\\csdtp2\\lib\\slf4j-api-1.5.8.jar;C:\\Users\\Shiro\\git\\csdtp2\\lib\\slf4j-jdk14-1.5.8.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\jersey\\core\\jersey-client\\2.25.1\\jersey-client-2.25.1.jar;C:\\Users\\Shiro\\.m2\\repository\\javax\\ws\\rs\\javax.ws.rs-api\\2.0.1\\javax.ws.rs-api-2.0.1.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\hk2\\hk2-api\\2.5.0-b32\\hk2-api-2.5.0-b32.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\hk2\\hk2-utils\\2.5.0-b32\\hk2-utils-2.5.0-b32.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\hk2\\external\\aopalliance-repackaged\\2.5.0-b32\\aopalliance-repackaged-2.5.0-b32.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\hk2\\external\\javax.inject\\2.5.0-b32\\javax.inject-2.5.0-b32.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\hk2\\hk2-locator\\2.5.0-b32\\hk2-locator-2.5.0-b32.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\javassist\\javassist\\3.20.0-GA\\javassist-3.20.0-GA.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\jersey\\core\\jersey-common\\2.25.1\\jersey-common-2.25.1.jar;C:\\Users\\Shiro\\.m2\\repository\\javax\\annotation\\javax.annotation-api\\1.2\\javax.annotation-api-1.2.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\jersey\\bundles\\repackaged\\jersey-guava\\2.25.1\\jersey-guava-2.25.1.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\hk2\\osgi-resource-locator\\1.0.1\\osgi-resource-locator-1.0.1.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\jersey\\core\\jersey-server\\2.25.1\\jersey-server-2.25.1.jar;C:\\Users\\Shiro\\.m2\\repository\\javax\\validation\\validation-api\\1.1.0.Final\\validation-api-1.1.0.Final.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\jersey\\media\\jersey-media-jaxb\\2.25.1\\jersey-media-jaxb-2.25.1.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\jersey\\containers\\jersey-container-jdk-http\\2.25.1\\jersey-container-jdk-http-2.25.1.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\jersey\\media\\jersey-media-json-jackson\\2.25.1\\jersey-media-json-jackson-2.25.1.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\glassfish\\jersey\\ext\\jersey-entity-filtering\\2.25.1\\jersey-entity-filtering-2.25.1.jar;C:\\Users\\Shiro\\.m2\\repository\\com\\fasterxml\\jackson\\jaxrs\\jackson-jaxrs-base\\2.8.4\\jackson-jaxrs-base-2.8.4.jar;C:\\Users\\Shiro\\.m2\\repository\\com\\fasterxml\\jackson\\core\\jackson-core\\2.8.4\\jackson-core-2.8.4.jar;C:\\Users\\Shiro\\.m2\\repository\\com\\fasterxml\\jackson\\core\\jackson-databind\\2.8.4\\jackson-databind-2.8.4.jar;C:\\Users\\Shiro\\.m2\\repository\\com\\fasterxml\\jackson\\jaxrs\\jackson-jaxrs-json-provider\\2.8.4\\jackson-jaxrs-json-provider-2.8.4.jar;C:\\Users\\Shiro\\.m2\\repository\\com\\fasterxml\\jackson\\module\\jackson-module-jaxb-annotations\\2.8.4\\jackson-module-jaxb-annotations-2.8.4.jar;C:\\Users\\Shiro\\.m2\\repository\\com\\fasterxml\\jackson\\core\\jackson-annotations\\2.8.4\\jackson-annotations-2.8.4.jar;C:\\Users\\Shiro\\.m2\\repository\\org\\json\\org.json\\chargebee-1.0\\org.json-chargebee-1.0.jar";
	SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();

	Process process;
	int id;
	String bad;

	Replica(int id, String bad) {
		this.id = id;
		this.bad = bad;
		System.out.println("Replica " + id + " " + bad);
		new Thread() {
			public void run() {
				for (;;) {
					try {
						BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				        String str = br.readLine();
				        if(str.equals("kill"))
				        	removeReplica();

						Thread.sleep(2000);
					} catch (InterruptedException | IOException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
		startReplica();
		
		try {

			Socket tmpSocket;
			DataInputStream is;
			DataOutputStream os;
			SSLContext sslContext = configureSSL();

			SSLServerSocketFactory serverFactory = sslContext.getServerSocketFactory();
			SSLServerSocket sslSocket = (SSLServerSocket) serverFactory.createServerSocket(PORT + id);

			for (;;) {
				tmpSocket = sslSocket.accept();
				is = new DataInputStream(tmpSocket.getInputStream());
				os = new DataOutputStream(tmpSocket.getOutputStream());
				switch (is.readInt()) {
				case ALIVE_REQUEST:
					os.writeBoolean(isAlive());
					break;
				case START_REQUEST:
					os.writeBoolean(startReplica());
					break;
				case REMOVE_REQUEST:
					os.writeBoolean(removeReplica());
					break;
				}
				os.flush();
				tmpSocket.close();
			}
		} catch (Exception e) {
			System.err.println("Error in Replica: " + e.getMessage());
			System.exit(0);
		}
	}

	private SSLContext configureSSL()
			throws UnknownHostException, NoSuchAlgorithmException, KeyStoreException, IOException, CertificateException,
			FileNotFoundException, UnrecoverableKeyException, KeyManagementException {
		System.out.println(InetAddress.getLocalHost().getHostAddress());

		SSLContext sslContext = SSLContext.getInstance("TLSv1.2");

		KeyStore keyStore = KeyStore.getInstance("JKS");
		try (InputStream iss = new FileInputStream(KEYSTORE)) {
			keyStore.load(iss, JKS_PASSWORD);
		}

		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(keyStore, KEY_PASSWORD);

		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(keyStore);

		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());
		return sslContext;
	}

	private boolean isAlive() {
		boolean alive = false;
		if(process != null)
			alive = process.isAlive();
		return alive;
	}

	boolean removeReplica() {
		boolean success = false;
		if (process != null && process.isAlive()) {
			process.destroy();
			process=null;
			System.out.println("Replica Removed");
		}
		return success;
	}

	boolean startReplica() {
		boolean success = false;
		if (process != null) {
			if (!process.isAlive())
				executeBftServer();

		} else
			executeBftServer();
		System.out.println("Replica Started");

		return success;
	}

	private boolean executeBftServer() {
		try {
			ProcessBuilder pbuilder = new ProcessBuilder();
			pbuilder.directory(new File(Paths.get(".").toAbsolutePath().normalize().toString()));

			pbuilder.command("java", "-classpath",CLASSPATH,"bftsmart.BftServer", Integer.toString(id), bad);
			pbuilder.inheritIO();
			process = pbuilder.start();

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static void main(String[] args) throws IOException {
		int idI = Integer.parseInt(args[0]);
		String b = "n";
		if (args[1].equals("y"))
			b = "y";
		
		new Replica(idI, b);
	}
}
