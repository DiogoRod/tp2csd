package srv;

import bftsmart.tom.AsynchServiceProxy;
import bftsmart.tom.RequestContext;
import bftsmart.tom.ServiceProxy;
import bftsmart.tom.core.messages.TOMMessage;
import bftsmart.tom.core.messages.TOMMessageType;
import bftsmart.tom.util.Extractor;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import bftsmart.BftRequestType;
import bftsmart.communication.client.ReplyListener;
import bftsmart.reconfiguration.Reconfiguration;
import bftsmart.reconfiguration.ServerViewController;
import bftsmart.reconfiguration.ViewManager;

@Path("/server")
public class ServerResource1 implements Extractor {

	HashMap<Integer, InetSocketAddress> repliesMap = new HashMap<Integer, InetSocketAddress>();
	LinkedList<Integer> repliesList = new LinkedList<Integer>();
	LinkedList<Integer> backupList = new LinkedList<Integer>();
	int[] list = new int[10];
	int[] rpl = new int[10];
	InetSocketAddress[] adresses;
	ServiceProxy clientProxy = null;
	AsynchServiceProxy asynchProxy = null;
	int clientId;
	ServerViewController controller;
	Reconfiguration rec;
	Thread th;
	
	Comparator<byte[]> comparator = new Comparator<byte[]>() {
		@Override
		public int compare(byte[] o1, byte[] o2) {
			return Arrays.equals(o1, o2) ? 0 : -1;
		}
	};
	private ReplyListener replyListener = new ReplyListener() {
		
		@Override
		public void replyReceived(RequestContext context, TOMMessage reply) {
			for (int i: clientProxy.getViewManager().getCurrentViewProcesses()) {
				if(i == reply.getSender())
					list[i] = 1;
			}
		}
	};

	protected ServerResource1(int bftId) {
		clientProxy = new ServiceProxy(bftId, "", comparator, this);
		asynchProxy = new AsynchServiceProxy(bftId+1, "", comparator, this);
		adresses = new InetSocketAddress[clientProxy.getViewManager().getStaticConf().getN() + 2];
		clientId = clientProxy.getViewManager().getStaticConf().getTTPId();
		controller = new ServerViewController(clientId);
		rec = new Reconfiguration(clientId);
		init();
	}

	private void init() {
		for (int i = 0; i < clientProxy.getViewManager().getStaticConf().getN() + 2; i++)
			System.out.println("Adding Address: "
					+ (adresses[i] = clientProxy.getViewManager().getStaticConf().getRemoteAddress(i)));
		for (int i : clientProxy.getViewManager().getCurrentViewProcesses())
			repliesList.add(i);
		backupList.add(clientProxy.getViewManager().getCurrentViewN());
		backupList.add(clientProxy.getViewManager().getCurrentViewN() + 1);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		th = new Thread() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(1000);
						list = new int[10];
						keepAlive();
						crashRecover();
					} catch (InterruptedException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};
		th.start();

	}

	@Path("/sets/{key}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getSet(@PathParam("key") String key) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.GET);
		dos.writeUTF(key);
		System.out.println("AQUI");
		try {
			byte[] reply = clientProxy.invokeUnordered(out.toByteArray());
			System.out.println("REPLY LENGTH" + reply.length);
			if (reply != null) {
				String replyS = new String(reply);
				System.out.println("REPLY GET : " + replyS);
				return replyS;
			} else
				return "null";
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Path("/sets/{key}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void putSet(@PathParam("key") String key, String entry) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.PUT);
		dos.writeUTF(key);
		dos.writeUTF(entry);
		byte[] reply = clientProxy.invokeOrdered(out.toByteArray());
		dos.close();
		ByteArrayInputStream in = new ByteArrayInputStream(reply);
		DataInputStream dis = new DataInputStream(in);
		boolean success = dis.readBoolean();
		System.out.println("Put Sucess?:" + success);
	}

	@Path("/sets/{key}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public void removeSet(@PathParam("key") String key) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.REMOVE);
		dos.writeUTF(key);
		byte[] reply = clientProxy.invokeOrdered(out.toByteArray());
		dos.close();
		ByteArrayInputStream in = new ByteArrayInputStream(reply);
		DataInputStream dis = new DataInputStream(in);
		boolean success = dis.readBoolean();
		System.out.println("Remove Sucess?:" + success);
	}

	@Path("/elems/{key}/{obj}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String readElem(@PathParam("key") String key, @PathParam("obj") String obj) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.READ);
		dos.writeUTF(key);
		dos.writeUTF(obj);
		byte[] reply = clientProxy.invokeUnordered(out.toByteArray());
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/elems/{key}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public void writeElem(@PathParam("key") String key, String obj) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.WRITE);
		dos.writeUTF(key);
		dos.writeUTF(obj);
		byte[] reply = clientProxy.invokeOrdered(out.toByteArray());
		dos.close();
		ByteArrayInputStream in = new ByteArrayInputStream(reply);
		DataInputStream dis = new DataInputStream(in);
		boolean success = dis.readBoolean();
		System.out.println("Write Sucess?:" + success);
	}

	@Path("/elems/{key}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public void addElem(@PathParam("key") String key, String obj) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.ADD);
		dos.writeUTF(key);
		dos.writeUTF(obj);
		byte[] reply = clientProxy.invokeOrdered(out.toByteArray());
		dos.close();
		ByteArrayInputStream in = new ByteArrayInputStream(reply);
		DataInputStream dis = new DataInputStream(in);
		boolean success = dis.readBoolean();
		System.out.println("Add Sucess?:" + success);

	}

	@Path("/elemsExist/{key}/{elem}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String isElement(@PathParam("key") String key, @PathParam("elem") String elem) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.ISELEM);
		dos.writeUTF(key);
		dos.writeUTF(elem);
		byte[] reply = clientProxy.invokeUnordered(out.toByteArray());
		String replyS = new String(reply);
		return replyS;

	}

	public void update(int id) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.UPDATE);
		dos.writeInt(id);
		clientProxy.invokeUnordered(out.toByteArray());
		dos.close();
	}

	@Override
	public TOMMessage extractResponse(TOMMessage[] replies, int same, int lastReceived) {
		TOMMessage last = replies[lastReceived];

		for (TOMMessage reply : replies) {
			if (reply != null) {
				if (comparator.compare(last.getContent(), reply.getContent()) != 0) {
					System.out.println("Replica " + reply.getSender() + " is bizantine");
					for (int j2 = 0; j2 < replies.length; j2++)
						System.err.println(clientProxy.getViewManager().getCurrentView().getProcesses()[j2] + " : "
								+ clientProxy.getViewManager().getCurrentView().getAddress(j2));
					changeView(reply.getSender());
				}
			}
		}
		return last;
	}

	private void crashRecover() {
		System.out.println("active:");
		for (int i : rpl)
			System.out.println(i);
		for (int i : clientProxy.getViewManager().getCurrentViewProcesses()) {
			if (list[i] != 1)
				rpl[i] += 1;
			else
				rpl[i] = 0;
			if (rpl[i] > 3) {
				System.out.println("ChangeView: " + i);
				changeView(i);
				for (int j = 0; j < 10; j++) {
					rpl = new int[10];
				}
			}
		}
	}

	private void changeView(int reply) {
		int id = backupList.removeFirst();
		backupList.add(reply);
		/*ViewManager vm = new ViewManager("");
		vm.connect();
		vm.removeServer(reply);
		vm.addServer(id, "127.0.0.1", 11000 + (id * 10));

		vm.executeUpdates();
		clientProxy.getViewManager().reconfigureTo(clientProxy.getViewManager().getViewStore().readView());
		asynchProxy.getViewManager().reconfigureTo(asynchProxy.getViewManager().getViewStore().readView());

		vm.close();
		asynchProxy.getCommunicationSystem().updateConnections();
		clientProxy.getCommunicationSystem().updateConnections();
		*/
		ViewManager vm = new ViewManager("");
		vm.connect();
		vm.removeServer(reply);
		vm.addServer(id, "127.0.0.1", 11000 + (id*10));
		/*try {
			update(reply.getSender());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		vm.executeUpdates();
		clientProxy.getViewManager().reconfigureTo((clientProxy.getViewManager().getCurrentView()));					
		vm.close();

		try {
			th.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void keepAlive() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.ALIVE);
		asynchProxy.invokeAsynchRequest(out.toByteArray(), replyListener, TOMMessageType.UNORDERED_REQUEST);

		dos.close();
	}
}
