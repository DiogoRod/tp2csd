package srv;

import bftsmart.tom.ServiceProxy;
import bftsmart.tom.core.messages.TOMMessage;
import bftsmart.tom.util.Extractor;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.zip.Deflater;
import bftsmart.BftRequestType;
import bftsmart.reconfiguration.ClientViewController;
import bftsmart.reconfiguration.ViewManager;

@Path("/server")
public class ServerResource implements Extractor {

	ServiceProxy clientProxy = null;
	Controller controller;
	LinkedList<Integer> backupList = new LinkedList<Integer>();
	ClientViewController vc;

	protected ServerResource(int bftId) {
		clientProxy = new ServiceProxy(bftId, "", null, this);
		controller = new Controller();
		vc = clientProxy.getViewManager();
		backupList.add(vc.getCurrentViewN());
		backupList.add(vc.getCurrentViewN() + 1);
		controller.removeReplica(backupList.getFirst());
		controller.removeReplica(backupList.getLast());
		new Thread() {
			public void run() {
				for (;;) {
					try {
						keepAlive();
						Thread.sleep(2000);
					} catch (InterruptedException | IOException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	@Path("/sets/{key}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getSet(@PathParam("key") String key) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.GET);
		dos.writeUTF(key);
		System.out.println("AQUI");
		try {
			byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
			System.out.println("REPLY LENGTH" + reply.length);
			if (reply != null) {
				String replyS = new String(reply);
				System.out.println("REPLY GET : " + replyS);
				return replyS;
			} else
				return "null";
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Path("/sets/{key}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void putSet(@PathParam("key") String key, String entry) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.PUT);
		dos.writeUTF(key);
		dos.writeUTF(entry);
		System.out.println(entry);
		byte[] reply = clientProxy.invokeOrdered(compress(out.toByteArray()));
		dos.close();
		/*ByteArrayInputStream in = new ByteArrayInputStream(reply);
		DataInputStream dis = new DataInputStream(in);
		return dis.readBoolean();*/
	}

	@Path("/test/{i}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public void test(@PathParam("i") int i) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.TEST);
		dos.writeInt(i);
		byte[] reply = clientProxy.invokeOrdered(compress(out.toByteArray()));
		dos.close();
		ByteArrayInputStream in = new ByteArrayInputStream(reply);
		DataInputStream dis = new DataInputStream(in);
		boolean success = dis.readBoolean();
		//System.out.println("Put Sucess?:"+success);
	}

	@Path("/sets/{key}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public void removeSet(@PathParam("key") String key) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.REMOVE);
		dos.writeUTF(key);
		byte[] reply = clientProxy.invokeOrdered(compress(out.toByteArray()));
		dos.close();
		ByteArrayInputStream in = new ByteArrayInputStream(reply);
		DataInputStream dis = new DataInputStream(in);
		boolean success = dis.readBoolean();
		// System.out.println("Remove Sucess?:"+success);
	}

	@Path("/elems/{key}/{obj}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String readElem(@PathParam("key") String key, @PathParam("obj") String obj) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.READ);
		dos.writeUTF(key);
		dos.writeUTF(obj);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/elems/{key}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public void writeElem(@PathParam("key") String key, String obj) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.WRITE);
		dos.writeUTF(key);
		dos.writeUTF(obj);
		byte[] reply = clientProxy.invokeOrdered(compress(out.toByteArray()));
		dos.close();
		ByteArrayInputStream in = new ByteArrayInputStream(reply);
		DataInputStream dis = new DataInputStream(in);
		boolean success = dis.readBoolean();
		// System.out.println("Write Sucess?:"+success);
	}

	@Path("/elems/{key}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public void addElem(@PathParam("key") String key, String obj) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.ADD);
		dos.writeUTF(key);
		dos.writeUTF(obj);
		byte[] reply = clientProxy.invokeOrdered(compress(out.toByteArray()));
		dos.close();
		ByteArrayInputStream in = new ByteArrayInputStream(reply);
		DataInputStream dis = new DataInputStream(in);
		boolean success = dis.readBoolean();
		// System.out.println("Add Sucess?:"+success);

	}

	@Path("/elemsExist/{key}/{elem}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String isElement(@PathParam("key") String key, @PathParam("elem") String elem) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.ISELEM);
		dos.writeUTF(key);
		dos.writeUTF(elem);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/sum/{key}/{key1}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String sum(@PathParam("key") String key, @PathParam("key1") String key1) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.SUM);
		dos.writeUTF(key);
		dos.writeUTF(key1);
		byte[] reply = clientProxy.invokeOrdered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/sumall")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String sumAll() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.SUMALL);
		byte[] reply = clientProxy.invokeOrdered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/mult/{key}/{key1}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String mult(@PathParam("key") String key, @PathParam("key1") String key1) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.MULT);
		dos.writeUTF(key);
		dos.writeUTF(key1);
		byte[] reply = clientProxy.invokeOrdered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/multAll")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String multAll() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.MULTALL);
		byte[] reply = clientProxy.invokeOrdered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/searcheq/{value}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String searchEq(@PathParam("value") String value) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.SEARCHEQ);
		dos.writeUTF(value);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		if (reply == null)
			return null;
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/searchNq/{value}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String searchNq(@PathParam("pos") String pos, @PathParam("value") String value) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.SEARCHNQ);
		dos.writeUTF(value);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/searchEntry/{value}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String searchEntry(@PathParam("value") String value) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.SEARCHENTRY);
		dos.writeUTF(value);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/searchOR/{value}/{value1}/{value2}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String searchOR(@PathParam("value") String value, @PathParam("value1") String value1,
			@PathParam("value2") String value2) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.SEARCHOR);
		dos.writeUTF(value);
		dos.writeUTF(value1);
		dos.writeUTF(value2);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/searchAND/{value}/{value1}/{value2}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String searchAND(@PathParam("value") String value, @PathParam("value1") String value1,
			@PathParam("value2") String value2) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.SEARCHAND);
		dos.writeUTF(value);
		dos.writeUTF(value1);
		dos.writeUTF(value2);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/orderLS")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String orderLS() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.ORDERLS);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/orderSL")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String orderSL() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.ORDERSL);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/searchGt/{value}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String searchGt(@PathParam("value") String value) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.SEARCHGT);
		dos.writeUTF(value);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/searchGtEq/{value}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String searchGtEq(@PathParam("value") String value) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.SEARCHGTEQ);
		dos.writeUTF(value);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/searchLt/{value}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String searchLt(@PathParam("value") String value) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.SEARCHLT);
		dos.writeUTF(value);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Path("/searchLtEq/{value}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String searchLtEq(@PathParam("value") String value) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(out);
		dos.writeInt(BftRequestType.SEARCHLTEQ);
		dos.writeUTF(value);
		byte[] reply = clientProxy.invokeUnordered(compress(out.toByteArray()));
		String replyS = new String(reply);
		return replyS;
	}

	@Override
	public TOMMessage extractResponse(TOMMessage[] replies, int same, int lastReceived) {
		TOMMessage last = replies[lastReceived];
		ArrayList<Integer> badReplicas = new ArrayList<Integer>();
		for (TOMMessage reply : replies) {
			if (reply != null && last != null) {
				if (!Arrays.equals(reply.getContent(), last.getContent())) {
					System.out.println("Replica " + reply.getSender() + " is bizantine");
					badReplicas.add(reply.getSender());
				}
			}
		}
		for (int j: badReplicas)
			changeView(j);

		return last;
	}

	private void changeView(int reply) {

		int id = backupList.removeFirst();
		backupList.add(reply);
		controller.removeReplica(reply);
		controller.startReplica(id);

		ViewManager vm = new ViewManager("");
		vm.removeServer(reply);
		vm.addServer(id, vc.getStaticConf().getHost(id), 11000 + (id * 10));
		System.out.println(vc.getStaticConf().getHost(id));
		vm.executeUpdates();

		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// vc.updateCurrentViewFromRepository();
		vm.close();
		clientProxy.getCommunicationSystem().updateConnections();
	}

	public void keepAlive() throws IOException {
		for (int i : vc.getCurrentViewProcesses()) {
			if (!backupList.contains(i))
				if (!controller.isAlive(i))
					changeView(i);
		}
	}
	public static byte[] compress(byte[] data) throws IOException {  
		   Deflater deflater = new Deflater();  
		   deflater.setInput(data);  
		   ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);   
		   deflater.finish();  
		   byte[] buffer = new byte[1024];   
		   while (!deflater.finished()) {  
		    int count = deflater.deflate(buffer); // returns the generated code... index  
		    outputStream.write(buffer, 0, count);   
		   }  
		   outputStream.close();  
		   byte[] output = outputStream.toByteArray();  
		   System.out.println("Original: " + data.length / 1024 + " Kb");  
		   System.out.println("Compressed: " + output.length / 1024 + " Kb");  
		   return output;  
		  }  
}
