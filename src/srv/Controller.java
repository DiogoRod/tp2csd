package srv;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Set;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import bftsmart.BftServer;

public class Controller {
	public static final int START_REQUEST = 1;
	public static final int REMOVE_REQUEST = 2;
	public static final int ALIVE_REQUEST = 0;
	public static final String IP = "192.168.1.6";
	public static final int PORT = 6060;
	static final File KEYSTORE = new File("./client.jks");
	static final char[] JKS_PASSWORD = "changeit".toCharArray();
	static final char[] KEY_PASSWORD = "changeit".toCharArray();
	static Set<String> availableKeys;
	HashMap<Integer, String> replicas = new HashMap<Integer,String>();
	SSLContext sslContext;
	

	
	protected Controller() {
		replicas.put(0, IP);
		replicas.put(1, IP);
		replicas.put(2, IP);
		replicas.put(3, IP);
		replicas.put(4, IP);
		replicas.put(5, IP);
		replicas.put(6, IP);
		replicas.put(7, IP);
		replicas.put(8, IP);
		
		try {
			configureSSL();
		} catch (UnrecoverableKeyException | KeyManagementException | NoSuchAlgorithmException | KeyStoreException
				| CertificateException | IOException e) {
			System.err.println("Error in Controller: " + e.getMessage());
		}

	}

	private void configureSSL() throws NoSuchAlgorithmException, KeyStoreException, IOException,
			CertificateException, FileNotFoundException, UnrecoverableKeyException, KeyManagementException {
		File keystore = KEYSTORE;
		sslContext = SSLContext.getInstance("TLSv1.2");
		
		KeyStore keyStore = KeyStore.getInstance("JKS");
		try (InputStream is = new FileInputStream(keystore)) {
			keyStore.load(is, JKS_PASSWORD);
		}

		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(keyStore, KEY_PASSWORD);
		
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(keyStore);

		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());
	}

	boolean isAlive(int replica) {
		boolean success = false;
		try {
			
			SSLSocketFactory factory = sslContext.getSocketFactory();
			SSLSocket socket = (SSLSocket) factory.createSocket(replicas.get(replica), PORT + replica);
			socket.startHandshake();
			DataOutputStream os = new DataOutputStream(socket.getOutputStream());
			DataInputStream is = new DataInputStream(socket.getInputStream());
			os.writeInt(ALIVE_REQUEST);
		//	System.out.println("Sent ALIVE_REQUEST to replica " + replica);
			os.flush();
			success = is.readBoolean();
			socket.close();

		} catch (Exception e) {
			System.err.println("Error in isAlive: " + e.getMessage());
			return success;
		}
		return success;
	}

	boolean removeReplica(int replica) {
		boolean success = false;
		try {
			
			SSLSocketFactory factory = sslContext.getSocketFactory();
			SSLSocket socket = (SSLSocket) factory.createSocket(replicas.get(replica), PORT + replica);
			socket.startHandshake();
			DataOutputStream os = new DataOutputStream(socket.getOutputStream());
			DataInputStream is = new DataInputStream(socket.getInputStream());
			os.writeInt(REMOVE_REQUEST);
			System.out.println("Sent REMOVE_REQUEST to replica " + replica);
			os.flush();
			success = is.readBoolean();
			socket.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}


	boolean startReplica(int replica) {
		boolean success = false;
		try {
			SSLSocketFactory factory = sslContext.getSocketFactory();
			SSLSocket socket = (SSLSocket)factory.createSocket(replicas.get(replica), PORT + replica);
			socket.startHandshake();
			DataOutputStream os = new DataOutputStream(socket.getOutputStream());
			DataInputStream is = new DataInputStream(socket.getInputStream());
			os.writeInt(START_REQUEST);
			System.out.println("Sent START_REQUEST to replica " + replica);
			os.flush();
			success = is.readBoolean();
			socket.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}
	public static void main(String[] args) throws IOException {
		
		new Controller();
	}
}