package clt;

import mlib.HomoAdd;
import mlib.HomoMult;
import mlib.PaillierKey;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;

public class tester1 {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InvalidKeySpecException, NoSuchAlgorithmException {
            int voltas = 1;
            //Generate two big integer probably primes with 128 bits
            KeyDispenser kd = new KeyDispenser();
            System.out.println("Generate two big integer probably primes with 128 bits");
            BigInteger big1 = new BigInteger("10");
            BigInteger big2 = new BigInteger("10");

            System.out.println("big1:     "+big1);
            System.out.println("big2:     "+big2);
            RSAPublicKey publicKey = kd.getPublicKey();
            RSAPrivateKey privateKey = kd.getPrivateKey();
            BigInteger big1Code = HomoMult.encrypt(publicKey, big1);


            int amostras = 1;
            for(int j = 0; j < amostras; j++){

                long startTime = System.currentTimeMillis();
                BigInteger big2Code = new BigInteger("1");
                for(int i = 0; i < voltas; i++){
                    big2Code = HomoMult.encrypt(publicKey, big2);
                }
                long endTime = System.currentTimeMillis();
                //System.out.println("Starting "+voltas+" RSA Decryptions of one Big Integer");
                startTime = System.currentTimeMillis();
                for(int i = 0; i < voltas; i++){
                    big2 = HomoMult.decrypt(privateKey, big2Code);
                }
                endTime = System.currentTimeMillis();
                //System.out.println("Resulting: in "+big2);
                BigInteger produto = new BigInteger("1");
                startTime = System.currentTimeMillis();
                for(int i = 0; i < voltas; i++){
                    produto = HomoMult.multiply(big1Code, big2Code, publicKey);
                }
                endTime = System.currentTimeMillis();
                System.out.println("Resulting: in "+HomoMult.decrypt(privateKey,produto));
            }

        }

    }


