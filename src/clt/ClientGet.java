package clt;

import java.io.*;
import java.math.BigInteger;
import java.net.URI;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

import mlib.*;
import org.json.*;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

public class ClientGet {
	static final File KEYSTORE = new File("./client.jks");
	static final char[] JKS_PASSWORD = "changeit".toCharArray();
	static final char[] KEY_PASSWORD = "changeit".toCharArray();
	static Set<String> availableKeys;
	static String[] names = { "Diogo", "David", "Sara", "Catarina", "Joao", "Pedro", "Joana", "Diana", "Fernando",
			"Luis" };

	static KeyDispenser kd;

	public static void main(String[] args) throws Exception {

		// String hostname = "192.168.1.4:9090";
		availableKeys = new HashSet<>();

		String hostname = "localhost:9090";
		kd = new KeyDispenser();
		File keystore = KEYSTORE;
		if (args.length > 0)
			hostname = args[0];
		if (args.length > 1)
			keystore = new File(args[1]);

		SSLContext sslContext = SSLContext.getInstance("TLSv1.2");

		KeyStore keyStore = KeyStore.getInstance("JKS");
		try (InputStream is = new FileInputStream(keystore)) {
			keyStore.load(is, JKS_PASSWORD);
		}

		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(keyStore, KEY_PASSWORD);

		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(keyStore);

		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());

		Client client = ClientBuilder.newBuilder().hostnameVerifier(new InsecureHostnameVerifier())
				.sslContext(sslContext).build();

		URI baseURI = UriBuilder.fromUri("https://" + hostname + "/").build();
		WebTarget target = client.target(baseURI);

		long start;
		start = test1(target);

		// long start = test2(availableKeys, target);

		System.out.println("Time: " + (System.currentTimeMillis() - start) / 1000f);
	}

	private static long test2(WebTarget target) throws Exception {
		long start = System.currentTimeMillis();

		// 1,419/1.511/
		System.out.println("Putting 20 entries");
		putEmployee(target, 20);
		System.out.println("Reading 20 entries");
		getSets(target, 20);
		System.out.println("Making everyone earn 100k!!");
		writeElem(target, 20);
		System.out.println("Reading a salary!");
		readElem(target, 20);
		System.out.println("Removing 5 entries..");
		removeSets(target, 5);
		System.out.println("Reading 20 entries");
		getSets(target, 15);
		System.out.println("Checking 20 entries if the name is Diogo");
		isElem(target, 20);
		System.out.println("Adding new Employee attributes");
		addElem(target, 50);

		return start;
	}

	private static long test1(WebTarget target) throws Exception {
		long start = System.currentTimeMillis();
		
		//test(target, 500);
		// PUT SETS 4.482/2.559/1.728/1.531/2.205/1.32
		putEmployee(target, 100);

		// GET SETS 0.58/0.785/0.482/0.467/0.451
		getSets(target, 100);

		for (int i = 0; i < 10; i++)
		orderLS(target);
		
		//System.out.println();
		searchEq(target, 10);
		// putEmployee(target, 50);

		// GET SETS 0.58/0.785/0.482/0.467/0.451
		// getSets(target, 50);
		 mult(target,10);
		 multAll(target);
		 searchLt(target,10);
		 searchLtEq(target,10);
		 searchGt(target,10);
		 searchGtEq(target,10);
		// searchGtEq(target,1);
		//getSets(target, 100);
		return start;
	}

	private static void test(WebTarget target,int n) {
		for (int i = 0; i < n; i++) {
			target.path("/server/test/"+i).request().accept(MediaType.APPLICATION_JSON).get();
		}
	}

	private static void sum(WebTarget target, int n) throws Exception {
		String responseS = "";
		for (int i = 0; i < n; i++) {
			responseS = target.path("/server/sum/" + getKey(i) + "/" + getKey(0)).request()
					.accept(MediaType.APPLICATION_JSON).get(String.class);
			System.out.println("Sum result: " + HomoAdd.decrypt(new BigInteger(responseS), kd.getPK()));
		}
	}

	private static void sumAll(WebTarget target) throws Exception {
		String responseS = "";
		responseS = target.path("/server/sumall").request().accept(MediaType.APPLICATION_JSON).get(String.class);
		System.out.println("Sum All result: " + HomoAdd.decrypt(new BigInteger(responseS), kd.getPK()));

	}

	private static void mult(WebTarget target, int n)
			throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		String responseM = "";
		for (int i = 0; i < n; i++) {
			responseM = target.path("/server/mult/" + getKey(i) + "/" + getKey(0)).request()
					.accept(MediaType.APPLICATION_JSON).get(String.class);
			System.out.println("Mult result: " + HomoMult.decrypt(kd.getPrivateKey(), new BigInteger(responseM)));
		}
	}

	private static void multAll(WebTarget target)
			throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		String responseMA = "";
		responseMA = target.path("/server/multAll").request().accept(MediaType.APPLICATION_JSON).get(String.class);
		System.out.println("Mult All result: " + HomoMult.decrypt(kd.getPrivateKey(), new BigInteger(responseMA)));
	}

	private static void searchEq(WebTarget target, int n) throws Exception {
		for (int i = 0; i < n; i++) {
		String responseSE = "";
		Random r = new Random();
			String name1 = names[r.nextInt(10)];
			System.out.println("Nome a procurar: " + name1);
			String cypher = HomoDet.encrypt(kd.getDetKey(), name1);
			try {
				responseSE = target.path("/server/searcheq/" + cypher).request().accept(MediaType.APPLICATION_JSON)
						.get(String.class);
				readJsonEmployees(responseSE);
			} catch (Exception e) {
				System.out.println("Found an error: " + e.getMessage() + " Retrying last operation...");
				// searchEq(target,1);
			}
		}
	}

	private static void searchNEQ(WebTarget target, int n) throws Exception {
		String responseSE = "";
		Random r = new Random();
		for (int i = 0; i < n; i++) {
			String name1 = names[r.nextInt(10)];
			System.out.println("Nome a procurar: " + name1);
			String cypher = HomoDet.encrypt(kd.getDetKey(), name1);
			try {
				responseSE = target.path("/server/searchNq/" + cypher).request().accept(MediaType.APPLICATION_JSON)
						.get(String.class);
			} catch (Exception e) {
				System.out.println("Found an error: " + e.getMessage());
			}

			readJsonEmployees(responseSE);
		}
	}

	private static void searchEntry(WebTarget target, int n) throws UnsupportedEncodingException {

	}

	private static void searchEntryOR(WebTarget target, int n) throws UnsupportedEncodingException {

	}

	private static void searchEntryAND(WebTarget target, int n) throws UnsupportedEncodingException {

	}

	private static void orderLS(WebTarget target) throws UnsupportedEncodingException, JSONException {
		String responseLS = "";
		responseLS = target.path("/server/orderLS").request().accept(MediaType.APPLICATION_JSON).get(String.class);
		JSONObject obj = new JSONObject(responseLS);
		HomoOpeInt ope = new HomoOpeInt(kd.getOpeKey());
		for (int i = 0; i < obj.length(); i++) {
			System.out.println(ope.decrypt(Long.parseLong(obj.getString("elem" + i))));
		}
	}

	private static void orderSL(WebTarget target) throws UnsupportedEncodingException, JSONException {
		String responseSL = "";
		responseSL = target.path("/server/orderSL").request().accept(MediaType.APPLICATION_JSON).get(String.class);
		JSONObject obj = new JSONObject(responseSL);
		HomoOpeInt ope = new HomoOpeInt(kd.getOpeKey());
		for (int i = 0; i < obj.length(); i++) {
			System.out.println(ope.decrypt(Long.parseLong(obj.getString("elem" + i))));
		}
	}

	private static void searchGt(WebTarget target, int n) throws Exception {
		String responseS = "";
		Random r = new Random();
		HomoOpeInt ope = new HomoOpeInt(kd.getOpeKey());

		for (int i = 0; i < n; i++) {
			int s = (r.nextInt(100000 - 20000 + 1) + 20000);
			System.out.println("Numero de comparacao: " + s);
			responseS = target.path("/server/searchGt/" + ope.encrypt(s)).request().accept(MediaType.APPLICATION_JSON)
					.get(String.class);
			readJsonEmployees(responseS);
		}
	}

	private static void searchGtEq(WebTarget target, int n) throws Exception {
		String responseS = "";
		Random r = new Random();
		HomoOpeInt ope = new HomoOpeInt(kd.getOpeKey());

		for (int i = 0; i < n; i++) {
			int s = (r.nextInt(100000 - 20000 + 1) + 20000);
			System.out.println("Numero de comparacao: " + s);
			responseS = target.path("/server/searchGtEq/" + ope.encrypt(s)).request().accept(MediaType.APPLICATION_JSON)
					.get(String.class);
			readJsonEmployees(responseS);
		}
	}

	private static void searchLt(WebTarget target, int n) throws Exception {
		String responseS = "";
		Random r = new Random();
		HomoOpeInt ope = new HomoOpeInt(kd.getOpeKey());

		for (int i = 0; i < n; i++) {
			int s = (r.nextInt(100000 - 20000 + 1) + 20000);
			System.out.println("Numero de comparacao: " + s);
			responseS = target.path("/server/searchLt/" + ope.encrypt(s)).request().accept(MediaType.APPLICATION_JSON)
					.get(String.class);
			System.out.println(responseS);
			readJsonEmployees(responseS);
		}
	}

	private static void searchLtEq(WebTarget target, int n) throws Exception {
		String responseS = "";
		Random r = new Random();
		HomoOpeInt ope = new HomoOpeInt(kd.getOpeKey());
		for (int i = 0; i < n; i++) {
			int s = (r.nextInt(100000 - 20000 + 1) + 20000);
			System.out.println("Numero de comparacao: " + s);
			responseS = target.path("/server/searchLtEq/" + ope.encrypt(s)).request().accept(MediaType.APPLICATION_JSON)
					.get(String.class);
			System.out.println(responseS);
			readJsonEmployees(responseS);
		}
	}

	private static void isElem(WebTarget target, int n) throws UnsupportedEncodingException {
		String responseIS = "";
		String cypherString = HomoDet.encrypt(kd.getDetKey(), "Diogo");

		for (int i = 0; i < n; i++) {
			if (availableKeys.size() != 0) {
				String key = getKey(i);
				responseIS = target.path("/server/elemsExist/" + key + "/" + cypherString).request()
						.accept(MediaType.APPLICATION_JSON).get(String.class);
				System.out.println("IsElement response: " + responseIS);
			}
		}
	}

	private static void readElem(WebTarget target, int n) {
		String responseRE = "";
		for (int i = 0; i < n; i++) {
			responseRE = target.path("/server/elems/" + getKey(i) + "/1").request().accept(MediaType.APPLICATION_JSON)
					.get(String.class);
		}
		long lk = kd.getOpeKey();
		HomoOpeInt ope = new HomoOpeInt(lk);
		long tempLong = Long.parseLong(responseRE);
		int salary = ope.decrypt(tempLong);

		System.out.println("Read response: " + salary);
	}

	private static void writeElem(WebTarget target, int n) throws JSONException {
		String replaceToken = generateReplaceToken();
		for (int i = 0; i < n; i++) {
			target.path("/server/elems/" + getKey(i)).request().accept(MediaType.APPLICATION_JSON)
					.put(Entity.entity(replaceToken, MediaType.APPLICATION_JSON));
		}
	}

	private static void removeSets(WebTarget target, int n) {
		for (int i = 0; i < n; i++) {
			String key = getKey(i);
			availableKeys.remove(key);
			target.path("server/sets/" + key).request().accept(MediaType.APPLICATION_JSON).delete();
		}
	}

	private static void addElem(WebTarget target, int n) throws JSONException {
		HomoOpeInt ope = new HomoOpeInt(kd.getOpeKey());
		Random r = new Random();

		for (int i = 0; i < n; i++) {
			int x = r.nextInt(100);
			long newValue = ope.encrypt(x);
			String newElem = Long.toString(newValue);

			target.path("/server/elems/" + getKey(i)).request().accept(MediaType.APPLICATION_JSON)
					.post(Entity.entity(newElem, MediaType.APPLICATION_JSON));
		}
	}

	private static void getSets(WebTarget target, int n) throws Exception {
		for (int i = 0; i < n; i++) {
			if (availableKeys.size() != 0) {
				String responseObj = target.path("/server/sets/" + getKey(i)).request()
						.accept(MediaType.APPLICATION_JSON).get(String.class);
				if (isJSONValid(responseObj)) {
					JSONObject obj = new JSONObject(responseObj);

					/*
					 * String[] temp = new String[200]; for(int
					 * j=0;j<obj.length();j++){ temp[i]=(String)
					 * obj.get("elem"+i); }
					 */

					String name = HomoDet.decrypt(kd.getDetKey(), obj.getString("elem0"));
					HomoOpeInt opeInt = new HomoOpeInt(kd.getOpeKey());
					long tempLong = Long.parseLong(obj.getString("elem1"));

					int salary = opeInt.decrypt(tempLong);

					KeyPair keyPair = kd.LoadKeyPair(kd.getPath(), "RSA");
					RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
					BigInteger accessLevel = HomoMult.decrypt(privateKey, new BigInteger(obj.getString("elem2")));

					BigInteger age = HomoAdd.decrypt(new BigInteger(obj.getString("elem3")), kd.getPK());

					System.out.println("Get response: " + name + " " + salary + " " + accessLevel + " " + age);
				} else
					System.out.println(responseObj);
			}
		}
	}

	private static void putEmployee(WebTarget target, int n) throws Exception {
		for (int i = 0; i < n; i++) {
			JSONObject employee = generateEmployee();
			String key = getHash(employee.toString());
			Iterator<String> it = employee.keys();
			String first = it.next();
			target.path("/server/sets" + "/" + key).request().accept(MediaType.APPLICATION_JSON)
					.put(Entity.entity(employee.toString(), MediaType.APPLICATION_JSON));
			availableKeys.add(key);
			/*while (it.hasNext()) {
				String next = it.next();
				target.path("/server/elems/" + key).request().accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity("{\""+next+"\":\""+employee.get(next)+"\"}", MediaType.APPLICATION_JSON));
			}*/
		}
	}

	private static String getKey(int i) {
		Iterator<String> it = availableKeys.iterator();
		String result = "no key";
		for (int j = 0; j <= i && it.hasNext(); j++) {
			if (it.hasNext()) {
				result = it.next();
			}
		}
		return result;
	}

	public static String generateReplaceToken() throws JSONException {
		JSONObject obj = new JSONObject();
		HomoOpeInt ope = new HomoOpeInt(kd.getOpeKey());
		long newValue = ope.encrypt(100000);
		String newStringValue = Long.toString(newValue);
		obj.put("elem0", 1);
		obj.put("elem1", newStringValue);
		return obj.toString();
	}

	public static void printAvailableKeys() {
		for (int i = 0; i < availableKeys.size(); i++) {
			System.out.println(availableKeys.toArray()[i]);
		}
	}

	public static JSONObject generateEmployee() throws Exception {

		// String keyString = "Bar12345Bar12345Bar12345";

		JSONObject obj = new JSONObject();
		Random r = new Random();

		String nome = names[r.nextInt(10)];

		String b = HomoDet.encrypt(kd.getDetKey(), nome);
		obj.put("elem0", b);

		long opeKey = kd.getOpeKey();
		HomoOpeInt ope = new HomoOpeInt(opeKey);
		int s = (r.nextInt(100000 - 20000 + 1) + 20000);
		long salary = ope.encrypt(s);
		String salaryString = (Long.toString(salary));

		obj.put("elem1", salaryString);

		int accessLevel = r.nextInt(10) + 10;
		BigInteger bigAccessLevel = BigInteger.valueOf(accessLevel);
		BigInteger accessCode = HomoMult.encrypt(kd.getPublicKey(), bigAccessLevel);
		String accessString = accessCode.toString();
		obj.put("elem2", accessString);

		int age = r.nextInt(100);
		BigInteger bigAge = BigInteger.valueOf(age);
		BigInteger ageCode = HomoAdd.encrypt(bigAge, kd.getPK());
		String ageString = ageCode.toString();
		obj.put("elem3", ageString);
		return obj;
	}

	public static void readJsonEmployees(String json) throws Exception {
		if (isJSONValid(json)) {
			JSONObject employessJ = new JSONObject(json);
			for (int j = 0; j < employessJ.length(); j++) {
				JSONObject obj = new JSONObject(employessJ.getString("elem" + j));
				System.out.println("obj " + j + obj.toString());
				String name = HomoDet.decrypt(kd.getDetKey(), obj.getString("elem0"));
				HomoOpeInt opeInt = new HomoOpeInt(kd.getOpeKey());
				long tempLong = Long.parseLong(obj.getString("elem1"));
				int salary = opeInt.decrypt(tempLong);
				BigInteger accessLevel = HomoMult.decrypt(kd.getPrivateKey(), new BigInteger(obj.getString("elem2")));
				BigInteger age = HomoAdd.decrypt(new BigInteger(obj.getString("elem3")), kd.getPK());
				System.out.println(
						"Name: " + name + " Salary: " + salary + " AccessLevel: " + accessLevel + " Age: " + age);
			}
		} else
			System.out.println(json);
	}

	private static String getHash(String entry) {
		String sha1 = "";

		try {
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");
			crypt.reset();
			crypt.update(entry.getBytes("UTF-8"));
			sha1 = Utils.toHex(crypt.digest());
		}

		catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return sha1;
	}

	static public class InsecureHostnameVerifier implements HostnameVerifier {
		@Override
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	}

	static public boolean isJSONValid(String test) {
		try {
			System.out.println("teste " + test);
			new JSONObject(test);
		} catch (JSONException ex) {
			// edited, to include @Arthur's comment
			// e.g. in case JSONArray is valid as well...
			try {
				new JSONArray(test);
			} catch (JSONException ex1) {
				return false;
			}
		}
		return true;
	}
}