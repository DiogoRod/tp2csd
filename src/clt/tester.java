package clt;

import mlib.HomoAdd;
import mlib.HomoDet;
import mlib.HomoOpeInt;
import mlib.PaillierKey;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by dcrod on 28/05/2017.
 */
public class tester {
    public static void main(String[] args) throws Exception {
        KeyDispenser kd = new KeyDispenser();
        HomoOpeInt ope = new HomoOpeInt(kd.getOpeKey());
        long cypher1 = ope.encrypt(12333);
        long cypher2 = ope.encrypt(333333);
        long cypher3 = ope.encrypt(3333333);
        long cypher4 = ope.encrypt(3333333);
        long cypher5 = ope.encrypt(77);
        long cypher6 = ope.encrypt(123);




        ArrayList<String> list = new ArrayList<>();
        list.add(String.valueOf(cypher1));
        list.add(String.valueOf(cypher2));
        list.add(String.valueOf(cypher3));
        list.add(String.valueOf(cypher4));
        list.add(String.valueOf(cypher5));
        list.add(String.valueOf(cypher6));

        System.out.println(list.get(1).equals(list.get(2)));
        HomoOpeInt ope1 = new HomoOpeInt();
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }

        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                HomoOpeInt ope1 = new HomoOpeInt();
                if(ope1.compare(Long.parseLong(o1),Long.parseLong(o2))){
                    return 1;
                }else if(o1.equals(o2)){
                    return 0;
                }
                else{
                    return -1;
                }
            }
        });
        System.out.println();

        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }

/*
        String cypher12 ="";
        String cypher13 ="";
        cypher12 = HomoDet.encrypt(kd.getDetKey(),"Diogo");
        cypher13 = HomoDet.encrypt(kd.getDetKey(),"Diogo");
        System.out.println(cypher12);
        System.out.println(cypher13);

        BigInteger big1 = new BigInteger("10");
        BigInteger big2 = new BigInteger("20");
        System.out.println(big1+" "+big2);
        BigInteger bigCode1 = new BigInteger("1");
        BigInteger bigCode2 = new BigInteger("1");

        bigCode1 = HomoAdd.encrypt(big1,kd.getPK());
        bigCode2 = HomoAdd.encrypt(big2,kd.getPK());
        String s = bigCode1.toString();
        String s1 = bigCode2.toString();

        BigInteger bigSum = new BigInteger("1");
        bigSum = HomoAdd.sum(new BigInteger(s),new BigInteger(s1),kd.getPK().getNsquare());

        BigInteger result = HomoAdd.decrypt(bigSum,kd.getPK());*/

        //System.out.println();
    }
}