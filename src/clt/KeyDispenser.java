package clt;

import mlib.*;

import javax.crypto.SecretKey;
import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * Created by dcrod on 25/05/2017.
 */
public class KeyDispenser {
    private PaillierKey pk;
    private SecretKey det;
    private long opeKey;
    private String path;

    public KeyDispenser() throws IOException, ClassNotFoundException {


        path = "C:\\Users\\Shiro\\git\\csdtp2";

        /*FileOutputStream fout = new FileOutputStream(path+"\\"+"paillierKey");
        ObjectOutputStream oos = new ObjectOutputStream(fout);
        oos.writeObject(HomoAdd.generateKey());
        oos.close();*/
        FileInputStream fin = new FileInputStream("C:\\Users\\Shiro\\git\\csdtp2\\src\\clt"+"\\"+"paillierKey");
        ObjectInputStream ois = new ObjectInputStream(fin);
        pk = (PaillierKey)ois.readObject();
        ois.close();

        det = HomoDet.generateKey(); // todo: guardar em ficheiro
        opeKey = HomoOpeInt.generateKey(); // todo: guardar em ficheiro

    }
    public RSAPublicKey getPublicKey() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        KeyPair kp = LoadKeyPair(path,"RSA");
        return (RSAPublicKey)kp.getPublic();
    }

    public RSAPrivateKey getPrivateKey() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        KeyPair kp = LoadKeyPair(path,"RSA");
        return (RSAPrivateKey) kp.getPrivate();
    }

    public PaillierKey getPK(){
        return pk;
    }

    public SecretKey getDetKey(){
        return det;
    }

    public long getOpeKey(){
        return opeKey;
    }

    public String getPath(){
        return path;
    }

    public String getNSq(){
        return pk.getNsquare().toString();
    }

    public static void SaveKeyPair(String path, KeyPair keyPair) throws IOException {
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();

        // Store Public Key.
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(
                publicKey.getEncoded());
        FileOutputStream fos = new FileOutputStream(path + "/public.key");
        fos.write(x509EncodedKeySpec.getEncoded());
        fos.close();

        // Store Private Key.
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(
                privateKey.getEncoded());
        fos = new FileOutputStream(path + "/private.key");
        fos.write(pkcs8EncodedKeySpec.getEncoded());
        fos.close();
    }
    public KeyPair LoadKeyPair(String path, String algorithm)
            throws IOException, NoSuchAlgorithmException,
            InvalidKeySpecException {
        // Read Public Key.
        File filePublicKey = new File(path + "/public.key");
        FileInputStream fis = new FileInputStream(path + "/public.key");
        byte[] encodedPublicKey = new byte[(int) filePublicKey.length()];
        fis.read(encodedPublicKey);
        fis.close();

        // Read Private Key.
        File filePrivateKey = new File(path + "/private.key");
        fis = new FileInputStream(path + "/private.key");
        byte[] encodedPrivateKey = new byte[(int) filePrivateKey.length()];
        fis.read(encodedPrivateKey);
        fis.close();

        // Generate KeyPair.
        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(
                encodedPublicKey);
        PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(
                encodedPrivateKey);
        PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

        return new KeyPair(publicKey, privateKey);
    }
}
